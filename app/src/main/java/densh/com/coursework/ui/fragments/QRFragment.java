package densh.com.coursework.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import densh.com.coursework.R;
import densh.com.coursework.ui.activities.qr.GenerationQRActivity;
import densh.com.coursework.ui.activities.qr.ScanningQRActivity;

/**
 * Created by densh on 12.11.2016.
 */
public class QRFragment extends Fragment {

    private CardView mOnResolveCard;
    private CardView mOnCreateCard;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_qr, container, false);

        mOnCreateCard = (CardView) view.findViewById(R.id.onCreateQRCodeCard);
        mOnResolveCard = (CardView) view.findViewById(R.id.onResolveQRCodeCard);

        mOnCreateCard.setOnClickListener(onCreateClick);
        mOnResolveCard.setOnClickListener(onResolveClick);

        return view;
    }

    View.OnClickListener onCreateClick = view -> startActivity(new Intent(getContext(), GenerationQRActivity.class));
    View.OnClickListener onResolveClick = view -> startActivity(new Intent(getContext(), ScanningQRActivity.class));
}
