package densh.com.coursework.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import densh.com.coursework.R;
import densh.com.coursework.ui.activities.barcode.GenerationBarcodeActivity;
import densh.com.coursework.ui.activities.barcode.ScanningBarcodeActivity;

/**
 * Created by densh on 12.11.2016.
 */
public class BarcodeFragment extends Fragment {

    private CardView mOnResolveCard;
    private CardView mOnCreateCard;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_barcode, container, false);

        mOnCreateCard = (CardView) view.findViewById(R.id.onCreateBarcodeCard);
        mOnResolveCard = (CardView) view.findViewById(R.id.onResolveBarcodeCard);

        mOnCreateCard.setOnClickListener(onCreateCode);
        mOnResolveCard.setOnClickListener(onResolveCode);
        return view;
    }

    View.OnClickListener onCreateCode = view -> startActivity(new Intent(getContext(), GenerationBarcodeActivity.class));
    View.OnClickListener onResolveCode = view -> startActivity(new Intent(getContext(), ScanningBarcodeActivity.class));
}
