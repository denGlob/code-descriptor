package densh.com.coursework.ui.activities.barcode;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.bottlerocketstudios.barcode.generation.ui.BarcodeView;
import com.google.zxing.BarcodeFormat;

import densh.com.coursework.R;

public class GenerationBarcodeActivity extends AppCompatActivity {

    private Handler mHandler = new Handler();
    private EditText mBarcodeText;
    private BarcodeView mBarcodeImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generation_barcode);

        mBarcodeText = (EditText) findViewById(R.id.generation_barcode_text);
        mBarcodeText.addTextChangedListener(mBarcodeTextWatcher);

        mBarcodeImage = (BarcodeView) findViewById(R.id.generation_barcode_image);
        mBarcodeImage.setBarcodeFormat(BarcodeFormat.EAN_13);
    }

    private TextWatcher mBarcodeTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            mHandler.removeCallbacks(mUpdateBarcodeRunnable);
            mHandler.postDelayed(mUpdateBarcodeRunnable, 500);
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    private Runnable mUpdateBarcodeRunnable = new Runnable() {
        @Override
        public void run() {
            mBarcodeImage.setBarcodeCharacterSet(mBarcodeText.getText().toString());
        }
    };
}
