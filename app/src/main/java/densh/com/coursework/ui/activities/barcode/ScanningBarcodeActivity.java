package densh.com.coursework.ui.activities.barcode;

import android.app.SearchManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;

import java.util.ArrayList;

import densh.com.coursework.R;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScanningBarcodeActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler{

    private static final String TAG = ScanningBarcodeActivity.class.getSimpleName();
    private ZXingScannerView mScannerView;
    private CheckBox mIsTorchEnabled;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanning_barcode);

        mScannerView = (ZXingScannerView) findViewById(R.id.scanner_view_barcode);
        mIsTorchEnabled = (CheckBox) findViewById(R.id.needTorchBarcode);
        additionalUtils();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    protected void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result result) {AlertDialog.Builder buildMessage = new AlertDialog.Builder(this);

        ArrayAdapter<String> adapterOut = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);
        adapterOut.clear();
        adapterOut.add(result.getText());
        adapterOut.add("Type: " + result.getBarcodeFormat().name());

        ArrayAdapter<String> adapterInner = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        buildMessage.setAdapter(adapterOut,
                (dialogInterface, i) ->
                {   adapterInner.clear();
                    adapterInner.add("Copy");
                    adapterInner.add("Find");
                    AlertDialog.Builder buildIn = new AlertDialog.Builder(this);
                    buildIn.setAdapter(adapterInner, (diaInt, pos) ->
                    {
                        if (adapterInner.getItem(pos).equals("Copy")) {
                            copyToClipboard(adapterOut.getItem(i));
                        }
                        else {
                            openLink(adapterOut.getItem(i));
                        }
                    }).show();
                }).show();
        // Note:
        // * Wait 2 seconds to resume the preview.
        // * On older devices continuously stopping and resuming camera preview can result in freezing the app.
        // * I don't know why this is the case but I don't have the time to figure out.
        Handler handler = new Handler();
        handler.postDelayed(() ->  mScannerView.resumeCameraPreview(ScanningBarcodeActivity.this), 2000);
    }

    private void additionalUtils() {
        mScannerView.setAutoFocus(true);
        ArrayList<BarcodeFormat> formats = new ArrayList<>(1);
        formats.add(BarcodeFormat.UPC_E);
        formats.add(BarcodeFormat.EAN_8);
        formats.add(BarcodeFormat.EAN_13);
        formats.add(BarcodeFormat.AZTEC);
        formats.add(BarcodeFormat.DATA_MATRIX);
        formats.add(BarcodeFormat.ITF);
        formats.add(BarcodeFormat.MAXICODE);
        mScannerView.setFormats(formats);
        mIsTorchEnabled.setOnCheckedChangeListener((compoundButton, b) -> mScannerView.setFlash(b));
    }

    private void openLink(String url) {
        if (isUrl(url)) {
            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(configureLink(url)));
            startActivity(i);
        }
        else {
            Intent i = new Intent(Intent.ACTION_WEB_SEARCH);
            i.putExtra(SearchManager.QUERY, url);
            startActivity(i);
        }

    }

    private void copyToClipboard(String copy) {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("label", copy);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(this, "Copied!", Toast.LENGTH_SHORT);
    }

    private boolean isUrl(String url) {
        return url.contains("http") || url.contains("www");
    }

    private String configureLink(String url) {
        if (!url.startsWith("http://") || !url.contains("https://")) {
            url = "http://" + url;
        }
        return url;
    }
}
