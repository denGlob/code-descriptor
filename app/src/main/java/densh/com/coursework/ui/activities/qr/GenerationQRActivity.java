package densh.com.coursework.ui.activities.qr;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.bottlerocketstudios.barcode.generation.ui.BarcodeView;

import densh.com.coursework.R;

public class GenerationQRActivity extends AppCompatActivity {

    private Handler mHandler = new Handler();
    private EditText mQRText;
    private BarcodeView mQRImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generation_qr);

        mQRText = (EditText) findViewById(R.id.generation_qr_text);
        mQRText.addTextChangedListener(mQRTextWatcher);

        mQRImage = (BarcodeView) findViewById(R.id.generation_qr_image);
    }

    private TextWatcher mQRTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            mHandler.removeCallbacks(mUpdateBarcodeRunnable);
            mHandler.postDelayed(mUpdateBarcodeRunnable, 500);
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    private Runnable mUpdateBarcodeRunnable = new Runnable() {
        @Override
        public void run() {
            mQRImage.setBarcodeText(mQRText.getText().toString());
        }
    };
}
